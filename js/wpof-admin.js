jQuery(document).ready(function($)
{
    $(".maj-wpof").click(function(e)
    {
        log = $("#log");
        bouton = $(this);
        
        jQuery.post
        (
            ajaxurl,
            {
                'action' : bouton.attr('data-action'),
                'param' : bouton.attr('data-param'),
            },
            function(response)
            {
                p = JSON.parse(response);
                log.html(p.log);
            },
        );
    });
    
});
