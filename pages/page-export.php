<?php
/*
 * page-export.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */
 
require_once(wpof_path . "/class/class-schema-export.php");

function wpof_load_export_scripts()
{
    wp_enqueue_script('wpof-export', wpof_url."js/wpof-export.js", array('jquery'));
    wp_enqueue_style('wpof-export', wpof_url."css/wpof-export.css");
}
 
add_filter( 'the_posts', 'generate_export_page', -10 );
function generate_export_page($posts)
{
    global $wp, $wp_query, $wpof;
    
    add_action( 'wp_enqueue_scripts', 'wpof_load_export_scripts', 21 );

    $url_slug = $wpof->url_export; // slug de la page d'export

    if (!defined('AIDE_PAGE') && (strtolower($wp->request) == $url_slug))
    {
        define( 'AIDE_PAGE', true );

        // create a fake virtual page
        $post = new stdClass;
        $post->post_author    = 1;
        $post->post_name      = $url_slug;
        $post->guid           = home_url() . '/' . $url_slug;
        $post->post_title     = $wpof->title_export;
        $post->post_content   = get_export_content();
        $post->ID             = -10;
        $post->post_type      = 'page';
        $post->post_status    = 'static';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->comment_count  = 0;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $posts                = NULL;
        $posts[]              = $post;

        // make wpQuery believe this is a real page too
        $wp_query->is_page             = true;
        $wp_query->is_singular         = true;
        $wp_query->is_home             = false;
        $wp_query->is_archive          = false;
        $wp_query->is_category         = false;
        unset( $wp_query->query[ 'error' ] );
        $wp_query->query_vars[ 'error' ] = '';
        $wp_query->is_404 = false;
    }

    return $posts;
}

function get_export_content()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
    $html = "";
    
    if (!in_array($role, array("um_formateur-trice", "um_responsable", "admin")))
        return "";
    
    $html .= "<div class='flexrow margin'>";
    // choix de la plage de dates
    $html .= get_choix_plage_date("get_tableau_sessions");
    
    // actions pour exporter
    $html .= get_action_export();
    
    $html .= "</div>";
    
    // onglets
    ob_start();
    ?>
    <div id="main-tabs">
    <ul>
            <li><a href="#tab-select"><?php _e("Sélection"); ?></a></li>
            <li><a href="#tab-cols"><?php _e("Colonnes"); ?></a></li>
    </ul>
        
        <div id="tab-select">
        <?php echo get_selection_export(); ?>
        </div>
        
        <div id="tab-cols">
        <?php echo get_colonnes_export(); ?>
        </div>
    
    <?php
    $html .= ob_get_clean();
    
    return $html;
}

function get_selection_export()
{
    $html = "";
    $html .= "<h2>".__("Sélection des sessions")."</h2>";
    $html .= get_tableau_sessions();
    
    return $html;
}

add_action('wp_ajax_get_tableau_sessions', 'get_tableau_sessions');
function get_tableau_sessions()
{
    global $wpof, $SessionFormation;
    $div_id = 'select_session';
    $reponse = array();
    
    $plage = array
    (
        'date_debut' => '01/01/'.$wpof->annee1,
        'date_fin' => date("d/m/Y", time() + (365 * 24 * 60 * 60)),
    );
    
    if (isset($_POST['plage']))
        $plage = $_POST['plage'];
    
    select_session_by_plage($plage);
    
    ob_start();
    ?>
    <div id='<?php echo $div_id; ?>'>
    
    <table class="opaga opaga2 selection_export export">
    <tr class="head">
    <th><input type="checkbox" name="all" checked="checked" data-list="<?php echo $div_id; ?>"/></th>
    <th><?php _e("Session"); ?></th>
    <th><?php _e("Dates"); ?></th>
    <th><?php _e("Client(s)"); ?></th>
    </tr>
    
    <?php foreach($SessionFormation as $session) : ?>
        <tr class="colonne">
        <td class="center"><input type="checkbox" name="<?php echo $session->id; ?>" checked="checked" /></td>
        <td><a href="<?php echo $session->permalien; ?>"><?php echo $session->titre_session; ?></a></td>
        <td class="center"><?php echo join("<br />", array_keys($session->creneaux)); ?></td>
        <td><?php echo $session->get_clients("nom", "<br />"); ?></td>
        </tr>
    <?php endforeach; ?>
    </table>
    
    </div>
    <?php
    $html = ob_get_clean();
    
    if (isset($_POST['action']))
    {
        $reponse['log'] = json_encode($_POST);
        $reponse['div_id'] = $div_id;
        $reponse['html'] = $html;
        echo json_encode($reponse);
        die;
    }
    else
        return $html;
}


function get_colonnes_export()
{
    ob_start();
    ?>
    <h2><?php _e("Choix des colonnes"); ?></h2>
    <div class="flexrow">
    <fieldset class="parent">
    <legend><?php _e("Schémas"); ?></legend>
    
    <?php
    echo get_schemas();
    echo get_save_new_schema();
    ?>
    </fieldset>
    
    <fieldset class="parent">
    <legend><?php _e("Actions"); ?></legend>
    <?php echo get_cols_actions(); ?>
    </fieldset>
    
    <fieldset class="parent">
    <legend><?php _e("Filtres"); ?></legend>
    <?php
    echo get_filtres();
    ?>
    </fieldset>
    </div>
    
    <?php
    $user_schemas = get_user_meta(get_current_user_id(), "schema_export", true);
    if (is_array($user_schemas) && count($user_schemas) > 0)
        $schema = reset($user_schemas);
    else
        $schema = new SchemaExport();
        
    echo $schema->get_choix_cols();
    ?>
    
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_action_export()
{
    ob_start();
    ?>
    <fieldset id="action_export">
    <legend><?php _e("Exporter en"); ?></legend>
    <div class="icone-bouton action_export" data-format="csv"><?php _e("CSV"); ?></div>
    </fieldset>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_filtres()
{
    ob_start();
    ?>
    <div id="filtres_export">
        <div class="flewrow">
        <div><?php _e("Afficher/cacher les colonnes non cochées"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_uncheck" data-list="cols-sortable"><?php _e("toutes"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="session" data-list="cols-sortable"><?php _e("session"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="client" data-list="cols-sortable"><?php _e("client"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="stagiaire" data-list="cols-sortable"><?php _e("stagiaire"); ?></div>
        <div class="icone-bouton cols_action" data-action="showhide_entite" data-entite="perso" data-list="cols-sortable"><?php _e("personnalisées"); ?></div>
        </div>
    </div>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

function get_cols_actions()
{
    ob_start();
    ?>
    <div id="cols_actions_export">
    <div class="icone-bouton cols_action" data-action="add_col" data-list="cols-sortable"><?php _e("Ajouter une colonne"); ?></div>
    </div>
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

add_action('wp_ajax_get_schemas', 'get_schemas');
function get_schemas($user_id = -1)
{
    if ($user_id <= 0)
        $user_id = get_current_user_id();
    
    $base_schemas = array();
    $base_schemas[] = new SchemaExport();
    
    ob_start();
    ?>
    <div class="schemas_dispo">
    <?php
    $user_schemas = get_user_meta($user_id, "schema_export", true);
    if (is_array($user_schemas)) :
        foreach($user_schemas as $sch) : ?>
            <div class="icone-bouton" data-schema="<?php echo $sch->id; ?>" data-userid="<?php echo $user_id; ?>" data-destination="#cols-sortable"><span class="select-schema"><?php echo $sch->nom; ?></span> <span class="delete-schema dashicons dashicons-dismiss"></span></div>
        <?php endforeach; ?>
    <?php endif; ?>
    <?php foreach($base_schemas as $sch) : ?>
        <div class="icone-bouton" data-schema="<?php echo $sch->id; ?>" data-userid="<?php echo $user_id; ?>" data-destination="#cols-sortable"><span class="select-schema"><?php echo $sch->nom; ?></span></div>
    <?php endforeach; ?>
    </div>
    <?php
    $html = preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
    if (isset($_POST['action']))
    {
        echo $html;
        die();
    }
    else
        return $html;
}

add_action('wp_ajax_active_schema', 'active_schema');
function active_schema()
{
    $schema = new SchemaExport($_POST['schema']);
    $schema->the_choix_cols_list();
    
    die();
}

function get_save_new_schema($user_id = -1)
{
    if ($user_id <= 0)
        $user_id = get_current_user_id();
    
    ob_start();
    ?>
    <label for="nom_schema"><?php _e("Nom du nouveau schéma"); ?></label> <input type="text" name="nom_schema" value="" /> <span class="icone-bouton enregistrer-schema" data-userid="<?php echo $user_id; ?>"><?php _e("Enregistrer"); ?></span>
    
    <?php
    return preg_replace('/(\r\n|\n\r|\n|\r)( )*/', '', ob_get_clean());
}

add_action('wp_ajax_opaga_export_data', 'opaga_export_data');
function opaga_export_data()
{
    $reponse = array();
    
    $export_path = WP_CONTENT_DIR . "/uploads/";
    
    $format = (isset($_POST['format'])) ? $_POST['format'] : "csv";
    
    $data = array();
    
    foreach($_POST['sessions'] as $sid)
    {
        $session = get_session_by_id($sid);
        foreach($session->clients as $cid)
        {
            $client = get_client_by_id($sid, $cid);
            foreach($client->stagiaires as $stag_id)
            {
                $stagiaire = get_stagiaire_by_id($sid, $stag_id);
                $row = array();
                
                foreach($_POST['cols'] as $c)
                {
                    $entite = $c['entite'];
                    switch($entite)
                    {
                        case "session":
                        case "client":
                        case "stagiaire":
                            $key = $c['db_text'];
                            if (isset($$entite->$key))
                            {
                                if (is_scalar($$entite->$key))
                                    $row[] = $$entite->$key;
                                else
                                    $row[] = serialize($$entite->$key);
                            }
                            else
                                $row[] = "";
                            break;
                        case "perso":
                            $row[] = $c['valeur'];
                            break;
                    }
                }
                $data[] = $row;
            }
        }
    }
    
    $head_row = array();
    foreach($_POST['cols'] as $c)
        $head_row[] = $c['new_text'];
    
    switch($format)
    {
        case 'csv':
            $csv_data = array();
            $csv_data[] = join(';', $head_row);
            foreach($data as $line)
                $csv_data[] = join(';', $line);
            $reponse['data'] = join("\r", $csv_data);
            $reponse['filename'] = "export_".date("Ymd-His").".$format";
            $reponse['mimetype'] = "text/csv";
            break;
    }
    
    echo json_encode($reponse);
    
    die();
}

?>
