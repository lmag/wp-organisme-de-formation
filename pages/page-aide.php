<?php
/*
 * page-aide.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

add_filter( 'the_posts', 'generate_aide_page', -10 );
function generate_aide_page($posts)
{
    global $wp, $wp_query, $wpof;

    $url_slug = $wpof->url_aide; // slug de la page d'aide

    if (!defined('AIDE_PAGE') && (strtolower($wp->request) == $url_slug))
    {
        define( 'AIDE_PAGE', true );

        // create a fake virtual page
        $post = new stdClass;
        $post->post_author    = 1;
        $post->post_name      = $url_slug;
        $post->guid           = home_url() . '/' . $url_slug;
        $post->post_title     = $wpof->title_aide;
        $post->post_content   = get_aide_content();
        $post->ID             = -10;
        $post->post_type      = 'page';
        $post->post_status    = 'static';
        $post->comment_status = 'closed';
        $post->ping_status    = 'closed';
        $post->comment_count  = 0;
        $post->post_date      = current_time( 'mysql' );
        $post->post_date_gmt  = current_time( 'mysql', 1 );
        $posts                = NULL;
        $posts[]              = $post;

        // make wpQuery believe this is a real page too
        $wp_query->is_page             = true;
        $wp_query->is_singular         = true;
        $wp_query->is_home             = false;
        $wp_query->is_archive          = false;
        $wp_query->is_category         = false;
        unset( $wp_query->query[ 'error' ] );
        $wp_query->query_vars[ 'error' ] = '';
        $wp_query->is_404 = false;
    }

    return $posts;
}

function get_aide_content()
{
    global $wpof;
    $role = wpof_get_role(get_current_user_id());
    
/*    if (!in_array($role, array("um_responsable", "admin")))
        return "";*/
    
    $aide_array = (array) $wpof->aide;
    ksort($aide_array);
    
    $html = "<p>".__("Compilation de toutes les bulles d'aide")." (".count($aide_array).")</p>";
    foreach($aide_array as $a)
    {
        $html .= "<div class='aide' style='display: flex;' id='{$a->slug}'>";
        $aide_objet = new Aide($a->slug);
        $html .= "<div style='width: 100px; font-size: 0.8em;'>".str_replace('_', ' ', $a->slug)."</div>";
        $html .= $aide_objet->get_aide(false);
        $html .= "</div>";
        //$html .= "</div>";
    }
    
    return $html;
}
