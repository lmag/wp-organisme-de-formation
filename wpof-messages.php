<?php
/*
 * wpof-messages.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


/*
 * Hooks pour Ultimate member
 */
add_filter( 'um_email_templates_path_by_slug', 'my_email_templates_path_by_slug', 15, 1 );
function my_email_templates_path_by_slug( $paths )
{
    $dir_path = wpof_path . "/template/ultimate-member/email";
    if ($handle = opendir($dir_path))
    {
        while (false !== ($entry = readdir($handle)))
        {
            if ($entry != "." && $entry != "..")
            {
                $slug = str_replace(".php", "", $entry);
                $paths[$slug] = $dir_path."/".$entry;
            }
        }
    }
    return $paths;
}

add_filter( 'um_locate_email_template', 'my_locate_email_template', 10, 2 );
function my_locate_email_template( $template, $template_name )
{
    // check if there is template at theme folder
    $blog_id = (is_multisite()) ? '/'.get_current_blog_id() : '';

    //get template file from current blog ID folder
    $template = locate_template( array(trailingslashit( 'ultimate-member/email' . $blog_id ) . $template_name . '.php') );

    //if there isn't template at theme folder for current blog ID get template file from theme folder
    if ( is_multisite() && ! $template )
    { 
        $template = locate_template( array(trailingslashit( 'ultimate-member/email' ) . $template_name . '.php') );
    }

    if (! $template)
    {
        $dir_path = wpof_path . "/template/ultimate-member/email";
        $template = $dir_path."/".$template_name.".php";
    }
    return $template;
}



add_filter( 'um_email_notifications', 'validation_docs_notifications', 10, 1 );
function validation_docs_notifications( $emails )
{
    // your code here
   $emails['validation_docs_email'] = array
   (
       'key'           => 'validation_docs_email',
       'title'         => __( 'Documents à valider par le responsable' ),
       'subject'       => 'Vous avez des documents à valider',
       'body'          => 'my_email_body',
       'description'   => 'Envoyé lorsque un formateur demande la validation d\'un document',
       'recipient'     => 'user', // set 'admin' for make administrator as recipient
       'default_active' => true // can be false for make disabled by default
    );

    return $emails;
}



?>
