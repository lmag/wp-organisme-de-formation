<?php
/*
 * wpof-utilisateur.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

$role_nom = array
(
    'um_stagiaire' => __('Stagiaire'),
    'um_formateur-trice' => __('Formateur⋅trice'),
    'um_responsable' => __('Responsable de formation'),
    'admin' => __('Administrateur⋅trice'),
);

function shortcode_show_liste_formateur( $atts )
{
    ob_start();
    // Attributes
    $atts = shortcode_atts(
        array
            (
            'cat' => 'all',
            'tag' => 'all',
            ),
	$atts
	);
	
    the_liste_formateur();
    
    return ob_get_clean();
    //debug_info($formateurs, "formateurs");
}
add_shortcode( 'liste_formateurs', 'shortcode_show_liste_formateur' );


function the_liste_formateur($atts = array(), $echo = true)
{
    $formateurs = get_formateurs($atts);
    $role = wpof_get_role(get_current_user_id());
    
    ob_start();
    ?>
    <ul class="list list_formateur">
    <?php
    foreach($formateurs as $f)
    {
        ?>
        <li><a href="<?php echo $f['permalink']; ?>"><?php echo $f['name']; ?></a><?php if (isset($f['brand']) && $f['brand'] != "") echo " (".$f['brand'].")"; ?></li>
        <?php
    }
    ?>
    </ul>
    <?php
    
    if ($echo)
        echo ob_get_clean();
    else
        return ob_get_clean();
}

function get_user_check_jpost($name, $label = "", $user_id)
{
    $html = "<div class='input_check_jpost'>";
    $input_id = $name.rand();
    
    if ($label != "")
        $html .= "<label class='input_check_jpost_label' for='$input_id'>$label</label>";
    $html .= "<input class='input_check_jpost_value' type='checkbox' id='$input_id' name='$name' value='1' ".checked(get_user_meta($user_id, $name, true), 1, false)." />";
    $html .= "<input type='hidden' name='user_id' value='{$user_id}' />";
    $html .= "</div>";
    
    return $html;
}

add_action( 'wp_ajax_update_user_value', 'update_user_value' );
function update_user_value()
{
    $value = ($_POST['checked'] == "true") ? 1 : 0;
    update_user_meta($_POST['user_id'], $_POST['meta'], $value);
    
    echo $_POST['checked'];
    echo $value;
    
    die();
}

function get_formateurs( $atts = array(), $single = false )
{
    global $wpof;
    
    if (in_array('only', array_keys($atts)))
    {
        foreach($atts['only'] as $id)
            $formateur_users[] = get_user_by("id", $id);
    }
    else
    {
        if (!$single)
            $formateur_users = get_users( array($atts, 'role__in' => array('um_formateur-trice', 'um_responsable') ) );
        else
            $formateur_users[0] = get_user( $atts['p'] );
    }
    
    
    //debug_info($formateur_users, "formateurs-list");
    $formateurs = array();
	
    foreach ($formateur_users as $fu)
    {
        $f = array();
        $f['user_id'] = $fu->ID;
        $f['user'] = $fu;
        $f['role'] = wpof_get_role($fu->ID);
        $f['name'] = $fu->data->display_name;
        if (champ_additionnel('formateur_marque'))
            $f['brand'] = get_user_meta($fu->ID, "formateur_marque", true);
        
        // Recherche du permalien de l'utilisateur
        $permalink_base = um_get_option('permalink_base');
        $f['permalink'] = "/user/".get_user_meta( $fu->ID, "um_user_profile_url_slug_{$permalink_base}", true );

        $formateurs[] = $f;
    }
    return $formateurs;
}


/*
 * Méta-fonction gérant l'affichage des tableaux de bord dans les profils utilisateurs
 */
function board_profil($user_id = null)
{
    $current_user_id = get_current_user_id();
    $user_id = ($user_id) ? $user_id : $current_user_id;

    $role = wpof_get_role($user_id);
    
    if ($user_id == $current_user_id)
    {
        if ($role != 'um_stagiaire')
            add_action( 'wp_enqueue_scripts', 'wp_enqueue_media', 16 );
        
        // Un admin ou responsable peut également être formateur ou stagiaire
        // Un formateur peut également être stagiaire
        
        switch($role)
        {
            case "admin":
            case "um_responsable":
            case "um_formateur-trice":
                board_profil_formateur($user_id, $role);
            case "um_stagiaire":
                board_profil_stagiaire($user_id, $role);
        }
    }
    else
        vue_publique($user_id, $role);
}

function board_profil_formateur($user_id, $role)
{
    global $SessionFormation;
    global $tinymce_wpof_settings;
    global $wpof;
    
    ?>
    <div class="board-formateur">
        <?php echo hidden_input("default_user_tab", (isset($_SESSION['user-tabs'])) ? $_SESSION['user-tabs'] : 0); ?>
        <div class="wpof-menu" id="user-tabs">
        <ul>
        <li><a href="#taches-form"><?php _e("Messages et tâches"); ?></a></li>
        <li><a href="#metadata-form"><?php _e("Votre profil de formateur-trice"); ?></a></li>
        <li><a href="#veille-form"><?php _e("Votre veille"); ?></a></li>
        <?php if (in_array($role, array("um_responsable", "admin"))) : ?>
            <li><a href="#comptes"><?php _e("Comptes formateurs/responsables"); ?></a></li>
        <?php endif; ?>
        </ul>
        
        <div id="taches-form" class="tableau">
            <?php
            if (is_signataire()) :
                $nb_docs = 0;
                $docs_a_valider = get_docs_to_validated(Document::VALID_RESPONSABLE_REQUEST);
                $nb_docs = count($docs_a_valider);
                ?>
                <?php if ($nb_docs > 0): ?>
                <p><?php _e("Voici la liste des documents à vérifier et, éventuellement, signer."); ?></p>
                
                <table class='gestion-docs-admin edit-data'><tbody>
                <tr class="tr-titre">
                    <th><?php _e("Document"); ?></th>
                    <th><?php _e("Finaliser"); ?></th>
                    <th><?php _e("Déposer le doc signé"); ?></th>
                </tr>
                
                <?php
                $last_session = 0;
                
                foreach ($docs_a_valider as $doc_line)
                {
                    if ($last_session != $doc_line->session_id)
                    {
                        $formateurs_nom = array();
                        $last_session = $doc_line->session_id;
                        $type_session = get_post_meta($last_session, 'type_formation', true);
                        $formateurs_id = get_post_meta($last_session, 'formateur', true);
                        foreach($formateurs_id as $f_id)
                        {
                            $formateurs_nom[] = get_displayname($f_id, false);
                        }
                        
                        echo "<tr><td colspan='3' class='td-intertitre'><a href='".get_the_permalink($doc_line->session_id)."'>".get_the_title($doc_line->session_id)."</a> ".__("animée par")." ".join(", ", $formateurs_nom)."</td></tr>";
                    }
                    $doc = new Document($doc_line->document, $doc_line->session_id, $doc_line->contexte, $doc_line->contexte_id);
                    $doc->signataire = true;
                    echo $doc->get_html_ligne(Document::COL_DOCUMENT | Document::COL_FINAL | Document::COL_SCAN | Document::COL_NOM_ENTITE);
                }
                ?>
                </tbody></table>
                <?php else: ?>
                <p><?php _e("Aucun document à signer. Passez une bonne journée."); ?></p>
                <?php
                endif; // if ($nb_docs > 0):
            endif; // if (is_signataire) :
            
            // Formateur
            $sessions = get_formation_sessions(array('formateur' => $user_id, 'quand' => 'futur', 'format' => 'html'));
            if ($sessions)
                echo "<h2>".__("Mes prochaines sessions")."</h2><p><em>".__("Contient également les sessions sans date")."</em></p>".$sessions;
            else
                echo "<p>".__("Aucune session programmée")."</p>";
                
            $sessions = get_formation_sessions(array('formateur' => $user_id, 'quand' => 'passe', 'format' => 'html'));
            if ($sessions)
                echo "<h2>".__("Mes sessions passées")."</h2>".$sessions;
            ?>
        </div>
        
        <div id="metadata-form" class="tableau metadata notif-modif">
            <?php echo get_icone_aide("utilisateur_profil", __("Renseignez votre profil")); ?>
            <div>
            <h3><?php _e("Photo"); echo get_icone_aide("utilisateur_photo"); ?></h3>
            <a href="#" class="clickButton button-add-media" data-valueid="photo" data-linkmedia="photo_link" data-image="photo_img"><?php _e("Téléversez une image"); ?></a>
            <?php $photo = get_user_meta($user_id, "photo", true); ?>
            <input type="hidden" id="photo" name="photo" value="<?php echo $photo; ?>" />
            <img id="photo_img" style="max-width: 300px; height: auto;" src="<?php echo wp_get_attachment_url($photo); ?>" />
            <a id="photo_link" target="_blank" href="<?php if ($photo != "") echo get_attachment_link($photo); ?>"><?php if ($photo != "") echo get_the_title($photo); ?></a>
            </div>
            <div>
            
            <h3><?php _e("CV"); echo get_icone_aide("utilisateur_cv"); ?></h3>
            <p>
            <a href="#" class="clickButton button-add-media" data-valueid="cv" data-linkmedia="cv_link" data-image="cv_img"><?php _e("Téléversez un document PDF"); ?></a>
            <?php $cv = get_user_meta($user_id, "cv", true); ?>
            <input type="hidden" id="cv" name="cv" value="<?php echo $cv; ?>" />
            <a id="cv_link" target="_blank" href="<?php if ($cv != "") echo wp_get_attachment_url($cv); ?>"><?php if ($cv != "") echo get_the_title($cv); ?></a>
            </p>
            <p>
            <label for="cv_url"><?php _e("Ou via un lien externe"); ?>
            <input type="text" size="60" id="cv_url" name="cv_url" value="<?php echo get_user_meta($user_id, "cv_url", true); ?>" /></label>
            </p>
            
            <?php if (champ_additionnel('formateur_marque')) : ?>
            <h3><?php _e("Marque"); echo get_icone_aide("utilisateur_marque"); ?></h3>
            <label for="formateur_marque"><?php _e("Votre marque ou nom commercial"); ?>
            <input type="text" size="60" id="formateur_marque" name="formateur_marque" value="<?php echo get_user_meta($user_id, "formateur_marque", true); ?>" /></label>
            <?php endif; ?>
            
            <h3><?php _e("Présentation"); echo get_icone_aide("utilisateur_presentation"); ?></h3>
            <?php
                $presentation = get_user_meta($user_id, "presentation", true);
                wp_editor($presentation, 'presentation', array_merge($tinymce_wpof_settings, array('textarea_rows' => 10, 'textarea_name' => 'presentation')));
            ?>
            <h3><?php _e("Réalisations"); echo get_icone_aide("utilisateur_realisations"); ?></h3>
            <?php
                $realisations = get_user_meta($user_id, "realisations", true);
                wp_editor($realisations, 'realisations', array_merge($tinymce_wpof_settings, array('textarea_rows' => 10, 'textarea_name' => 'realisations')));
            ?>
            </div>
            <p data-userid="<?php echo $user_id; ?>" class="bouton submit enregistrer-user-input"><?php _e("Enregistrer les informations"); ?></p>
            <p class="message"></p>
        </div>
        
        <div id="veille-form" class="metadata notif-modif">
            <?php echo get_icone_aide("utilisateur_veille", __("Veille sur la formation")); ?>
            <p><?php _e('Notez ici toutes vos actions de veille : lecture de sites, livres, réseaux, lettres d’info, participations à des conférences, événements, etc. Pensez à dater !'); ?></p>
            <?php
            $veille_bloc_notes = get_user_meta($user_id, "veille_bloc_notes", true);
            wp_editor($veille_bloc_notes, 'veille_bloc_notes', array_merge($tinymce_wpof_settings, array('textarea_rows' => 20, 'textarea_name' => 'veille_bloc_notes')));
            ?>
            <p data-userid="<?php echo $user_id; ?>" class="bouton submit enregistrer-user-input"><?php _e("Enregistrer les informations"); ?></p>
            <p class="message"></p>
        </div>
        
        <?php if (in_array($role, array("um_responsable", "admin"))) : ?>
        <div id="comptes" class="tableau">
        <?php echo get_add_compte_form(); ?>
        <?php echo get_gestion_comptes(); ?>
        </div>
        <?php endif; ?>
        
        </div> <!-- user-tabs -->
    
    
    </div> <!-- board-formateur -->
    <?php
}

function board_profil_stagiaire($user_id, $role)
{
    global $etat_session;
    global $SessionFormation;
    global $SessionStagiaire;
    
    ?>
    <div class="board-stagiaire">
    
    <?php
    $user_meta = get_user_meta($user_id);
    
    //debug_info($user_meta, "meta");
    
    $sessions = get_user_meta($user_id, "inscription", true);
    if ($sessions == "") $sessions = array();
    
    //debug_info($sessions, "sessions");
    if (count($sessions) > 0) :
    ?>
    
    <h2><?php _e("Mes inscriptions à des sessions de formation"); ?></h2>
    <ul>
    
    <?php
    
    foreach($sessions as $session_id)
    {
        $s = new SessionStagiaire($session_id, $user_id);
        echo "<li><a href='{$SessionFormation[$session_id]->permalien}'>{$SessionFormation[$session_id]->titre_session}</a><br />";
        echo __("État de votre inscription")." : ".$etat_session[$s->etat_session]."</li>";
    }
    ?>
    </ul>
    <?php
    else :
        if ($role == 'um_stagiaire')
            echo "<p>".__("Vous n'êtes inscrit à aucune session")."</p>";
        
    endif;
    ?>
    
    
    </div> <!-- board-stagiaire -->
    <?php
}

/*
 * Vue publique d'un utilisateur par quelqu'un d'autre que lui-même
 */
function vue_publique($user_id, $role_visiteur)
{
    if ($role = wpof_get_role($user_id))
    {
        global $role_nom;
        global $wpof;
        
        ?>
        <div class="presentation-profil">
        <?php
        /*echo "<p id='role'>".$role_nom[$role];
        if (is_signataire($user_id))
            echo " ".__("signataire des documents administratifs");
        echo "</p>";*/
        
        switch($role)
        {
            case "um_responsable":
            case "um_formateur-trice":
                echo "<div id='description'>";
                if ($photo = get_user_meta($user_id, "photo", true))
                    echo "<img id='photo' src='".wp_get_attachment_url($photo)."' />";
                echo get_user_meta($user_id, "presentation", true);
                echo "</div>";
                
                if (champ_additionnel('formateur_marque') && $formateur_marque = get_user_meta($user_id, "formateur_marque", true))
                    echo "<h3>".__("Marque")."</h3><p id='marque'>$formateur_marque</p>";
        
                if ($realisations = get_user_meta($user_id, "realisations", true))
                    echo "<div id='realisations'><h3>".__("Ses réalisations")."</h3>".$realisations."</div>";
            
                echo "<div id='formations'>";
                $formations = get_formations(array('formateur' => $user_id, 'format' => 'html'));
                if ($formations)
                    echo "<h2>".__("Son catalogue de formations")."</h2>".$formations;
                echo "</div>";
                
                echo "<div id='sessions'>";
                $sessions = get_formation_sessions(array('formateur' => $user_id, 'quand' => 'futur', 'format' => 'html'));
                if ($sessions)
                    echo "<h2>".__("Ses prochaines sessions")."</h2>".$sessions;
                    
                $sessions = get_formation_sessions(array('formateur' => $user_id, 'quand' => 'passe', 'format' => 'html'));
                if ($sessions)
                    echo "<h2>".__("Ses sessions passées")."</h2>".$sessions;
                echo "</div>";
                
                break;
        }
        ?> </div> <?php
    }
    else
        echo "<p class='erreur'>".__("Utilisateur inexistant !")."</p>";
}


/*
 * La méthode get_role d'Ultimate member renvoie le rôle WP s'il existe et pas le rôle UM
 * Cette fonction vise à palier à ce manque en ne renvoyant QUE les rôles utilisés dans le plugin
 */
function wpof_get_role($user_id)
{
    if ($user_id == 0)
        return false;

    if (user_can($user_id, "um_stagiaire"))
        return "um_stagiaire";

    if (user_can($user_id, "um_formateur-trice"))
        return "um_formateur-trice";

    if (user_can($user_id, "um_responsable"))
        return "um_responsable";
    
    if (user_can($user_id, 'manage_options'))
        return "admin";
}

/*
 * Ajouter un utilisateur
 * Analyse des données du formulaire
 */
add_action( 'wp_ajax_add_stagiaire', 'add_stagiaire' );
function add_stagiaire()
{
    global $ultimatemember;
    
    $reponse = array('message' => '', 'stagiaires_board' => '', 'html' => '');
    
    $user = array();
    $session = null;
    $session_id = 0;
    
    // Analyse des données
    if (isset($_POST['data']))
    {
        $data = json_decode(html_entity_decode(stripslashes($_POST['data'])));
        $user = $data->allusers;
        $session_id = $data->session_id;
    }
    else
    {
        $user[0] = (object) array();
        foreach(array('genre', 'firstname', 'lastname', 'email', 'role') as $field)
            $user[0]->$field = (isset($_POST[$field])) ? $_POST[$field] : "";
        if (isset($_POST['session_id'])) $session_id = $_POST['session_id'];
        if (isset($_POST['client_id'])) $client_id = $_POST['client_id'];
    }
        
    if ( isset($_FILES["file"]) && $_FILES["file"]["type"] == 'text/csv')
    {
        if ($_FILES["file"]["error"] > 0)
        {
            $reponse['message'] .= "<span class='erreur'>".__("Erreur de fichier CSV")." ".$_FILES["file"]["error"]."</span>";
        }
        else
        {
            $fhandle = fopen($_FILES["file"]["tmp_name"], "r");
            
            // première ligne ignorée
            $line = fgetcsv($fhandle, 0, ';');
            
            $i = count($user);
            while ($line = fgetcsv($fhandle, 0, ';'))
            {
                $line = array_map("trim", $line);
                $line = array_pad($line, 5, "");
                $line[0] = strtoupper($line[0]);
                $line[4] = strtoupper($line[4]);
                $line[1] = ucfirst($line[1]);
                $line[2] = ucfirst($line[2]);
                
                if (count($line) < 3 || $line[0] == "" || $line[1] == "" || $line[2] == "" || !in_array($line[0], array("F", "H")))
                {
                    $reponse['message'] .= "<p><span class='erreur'>".__("Informations manquantes ou erronées")." ".join(";", $line)."</span></p>";
                    continue;
                }
                $user[$i] = new stdClass();
                $user[$i]->genre = ($line[0] == "H") ? "Monsieur" : "Madame";
                $user[$i]->firstname = $line[1];
                $user[$i]->lastname = $line[2];
                $user[$i]->email = $line[3];
                switch ($line[4])
                {
                    case "R":
                        $user[$i]->role = "um_responsable";
                        break;
                    case "F":
                        $user[$i]->role = "um_formateur-trice";
                        break;
                    case "S":
                    default:
                        $user[$i]->role = "um_stagiaire";
                        break;
                }
                $i++;
            }
            fclose($fhandle);
        }
    }
    foreach ($user as $u)
    {
        if ($u->email == "" && $u->role == "um_stagiaire")
            $u->email = sanitize_user( strtolower( str_replace( " ", ".", $u->firstname . " " . $u->lastname )))."@".date_i18n("Ymj")."fausseadres.se";
        if ($u->firstname != "" && $u->lastname != "" && $u->email != "" )
        {
            $reponse['message'] .= "<p>{$u->genre} {$u->firstname} {$u->lastname} ({$u->email}) ";
            $userdata = array
            (
                'first_name' => $u->firstname,
                'last_name' => $u->lastname,
                'user_email' => $u->email,
                'user_login' => sanitize_user( strtolower( str_replace( " ", ".", $u->firstname . " " . $u->lastname ) ) ),
                'user_pass' => NULL
            );
            $user_id = wp_insert_user($userdata);
            if (is_wp_error($user_id))
            {
                $reponse['message'] .= "<span class='erreur'>".$user_id->get_error_message()."</span>";
            }
            else
            {
                add_user_meta($user_id, 'genre', $u->genre);
                $user_obj = new WP_User($user_id);
                $user_obj->set_role($u->role);
                    
                //UM()->mail()->send($u->email, 'welcome_email'); // cette action est déjà effectuée par UM
                $reponse['message'] .= "<span class='succes'>".__("compte créé")."</span>";
            
                if ($session_id > 0)
                {
                    $session = get_session_by_id($session_id);
                    
                    // Ajout de l'ID de la session de formation dans la meta inscription du stagiaire
                    $inscription = get_user_meta($user_id, "inscription", true);
                    if ($inscription == "")
                        $inscription = array();
                    
                    $inscription[] = $session_id;
                    update_user_meta($user_id, "inscription", array_unique($inscription));
                    
                    $session->inscrits[] = $user_id;
                    update_post_meta($session_id, 'inscrits', $session->inscrits);
                        
                    if ($client_id > 0)
                    {
                        $client = get_client_by_id($session_id, $client_id);
                        $client->stagiaires[] = $user_id;
                        $client->update_meta("stagiaires", $client->stagiaires);
                    }
                    else
                    {
                        global $SessionStagiaire;
                        $SessionStagiaire[$user_id] = new SessionStagiaire($session_id, $user_id);
                        ob_start();
                        $session->the_stagiaire_board($user_id);
                        $reponse['stagiaires_board'] .= ob_get_clean();
                    }
                    $reponse['message'] .= " — <span class='succes'>".__("inscrit à la session")." ".$session->titre_session."</span>";
                }
            }
            $reponse['message'] .= "</p>";
        }
    }
    if ($reponse['message'] == "")
    {
        $reponse['message'] .= "<span class='erreur'>".__("Saisissez un prénom et un nom pour inscrire un⋅e stagiaire")."</span>";
        $reponse['erreur'] = $reponse['message'];
    }
    else
    {
        $reponse['succes'] = $reponse['message'];
        if ($session)
            $reponse['html'] = $session->get_tabs_clients();
    }
    

    echo json_encode($reponse);
    
    die();
}

/*
 * Ajouter un utilisateur
 * Affichage du formulaire
 * $session_id : si > 0 alors les stagiaires sont également inscrits à cette session
 */
function get_add_compte_form($session_id = 0, $client_id = 0)
{
    ob_start();
    ?>
    <div id="add_compte">
        <?php echo get_icone_aide("utilisateur_ajout_compte", __("Créer des comptes")); ?>
        <div id="add_compte_form" class="dynamic_form notif-modif">
        <input type="hidden" name="session_id" value="<?php echo $session_id; ?>" />
        <input type="hidden" name="client_id" value="<?php echo $client_id; ?>" />
        
        <table class="add-compte">
        <tbody>
        
        <tr id='0_new-stagiaire' data-num='0' class='new-stagiaire'>
        <td><select name="0_genre"><option value="Madame">Madame</option><option value="Monsieur">Monsieur</option></select></td>
        <td><input type="text" name="0_firstname" placeholder="<?php _e("Prénom"); ?>" /></td>
        <td><input type="text" name="0_lastname" placeholder="<?php _e("Nom"); ?>" /></td>
        <td><input type="text" name="0_email" placeholder="<?php _e("Courriel"); ?>" /></td>
        <?php
        $data_selectrole = "";
        if ($session_id == 0) :
            global $role_nom;
            $role_list = $role_nom;
            unset($role_list['admin']);
            unset($role_list['um_stagiaire']);
            $select_role = select_by_list($role_list, "NUM_role", "um_formateur-trice");
        ?>
        <td><?php echo str_replace("NUM", "0", $select_role); ?></td>
        <?php
            // on doit passer la liste des rôles seulement dans le cas où $session_id vaut 0 (c'est-à-dire si cette boîte est affichée hors d'une session)
            $data_selectrole = "data-selectrole='".json_encode($role_list)."'";
            endif;
        ?>
        <td><span class="dashicons dashicons-plus-alt icone-bouton" <?php echo $data_selectrole; ?> id="plus"></span></td>
        </tr>
        
        </tbody>
        </table>
        
        <input type='hidden' name='action' value='add_stagiaire' />
        
        <p class="bouton submit" id="add-compte-bouton"><?php _e("Ajouter"); ?></p>
        <p class="message"></p>
        </div>
        
        <?php //if (in_array(wpof_get_role(get_current_user_id()), array("um_responsable", "admin"))) : ?>
        <?php //if (in_array(wpof_get_role(get_current_user_id()), array("admin"))) : ?>
        <?php if (0) : ?>
            <div id="add_compte_csv">
            <form id="add-stagiaire-csv-form" method="POST" name="add-stagiaire-csv-form" enctype="multipart/form-data">
                <label for="add-stagiaire-csv"><?php _e("Listing au format CSV avec comme colonnes : genre (F ou H) ; prenom ; nom ; email ; role (R ou F ou S). La première ligne (souvent utilisée pour caractériser les colonnes) est <strong>ignorée</strong>"); ?></label><br />
                <input id="add-stagiaire-csv" name="add-stagiaire-csv" type="file" accept=".csv" />
                <input class="ajax-save-file bouton" data-action="add_stagiaire" data-sessionid="<?php echo $session_id; ?>" data-message-parent="add_compte_form" type="button submit" value="<?php _e("Traiter le CSV"); ?>" />
            </form>
            <p class="message"></p>
            </div>
        <?php endif; ?>
        
    </div>
    <?php
    return ob_get_clean();
}


function get_gestion_comptes()
{
    global $role_nom;
    $role = wpof_get_role(get_current_user_id());
    $formateurs = get_formateurs();
    
    $formations = get_formations();
    $anime_formation = array();
    if (!empty($formations))
    {
        foreach($formations as $f)
            foreach($f->formateur as $user_id)
                $anime_formation[$user_id][] = "<a href='{$f->permalien}'>{$f->id}</a>";
    }
            
    $sessions = get_formation_sessions();
    $anime_session = array();
    if (!empty($sessions))
    {
        foreach($sessions as $s)
            foreach($s->formateur as $user_id)
                $anime_session[$user_id][] = "<a href='{$s->permalien}'>{$s->id}</a>";
    }
    
    ob_start();
    ?>
    <table class="gestion_formateurs opaga opaga2">
    
    <tr>
    <th><?php _e("Nom"); ?></th>
    <th><?php _e("Rôle"); ?></th>
    <th><?php _e("Sous-traitant"); ?></th>
    <th><?php _e("Mentionné dans"); ?></th>
    <th><?php _e("Modifier"); ?></th>
    <th><?php _e("Supprimer"); ?></th>
    <?php if ($role == "admin") : ?>
        <th><?php _e("Usurper"); ?></th>
    <?php endif; ?>

    
    <?php foreach($formateurs as $f) : ?>
    <tr id="user<?php echo $f['user_id']; ?>">
    <td><a href="<?php echo $f['permalink']; ?>"><?php echo $f['name']; ?></a></td>
    <td><?php echo $role_nom[$f['role']]; ?></td>
    <td class="center"><?php echo get_user_check_jpost("sous_traitant", "", $f['user_id']); ?></td>
    <td><?php
        if (isset($anime_formation[$f['user_id']]))
            echo "<p>".__("Formations")." : ".join(', ', $anime_formation[$f['user_id']])."</p>";
        if (isset($anime_session[$f['user_id']]))
            echo "<p>".__("Sessions")." : ".join(', ', $anime_session[$f['user_id']])."</p>";
    ?></td>
    <td class="center"><a href="<?php echo home_url(); ?>/wp-admin/user-edit.php?user_id=<?php echo $f['user_id']; ?>"><span class="dashicons dashicons-edit-large"></span></a></td>
    <td class="center"><span class="pointer dashicons dashicons-dismiss delete-entity" data-objectclass="Utilisateur" data-id="<?php echo $f['user_id']; ?>" data-parent="#user<?php echo $f['user_id']; ?>"></span></td>
    <?php if ($role == "admin"): ?>
        <td class="center"><span data-userid='<?php echo $f['user_id']; ?>' class='pointer switch-user dashicons dashicons-controls-repeat'></span></td>
    <?php endif; ?>
    </tr>
    <?php endforeach; ?>
    
    </table>
    <?php
    return ob_get_clean();
}

/*
 * Renvoie un stagiaire d'une session en sous-traitance sous la forme
 * <input nom> <input nb_heure>
 */
function get_st_stagiaire($session_id, $nb_heure = 0, $stagiaire_id = -1)
{
    if ($stagiaire_id == -1)
    {
        $nom = "Stagiaire ".rand();
    }
    else
    {
        $session_stagiaire = new SessionStagiaire($session_id, $stagiaire_id);
        $nom = $session_stagiaire->user->user_nicename;
        $nb_heure = $session_stagiaire->nb_heure_decimal;
    }
    
    $rand_id = rand();
    ob_start();
    ?>
    <tr>
    <td>
        <input type="hidden" name="stag_id" value="<?php echo $stagiaire_id; ?>" />
        <input type="hidden" name="stag_session_id" value="<?php echo $session_id; ?>" />
        <input type="text" name="stag_nom" value="<?php echo $nom; ?>" />
    </td>
    <td><input type="number" step="0.25" min="0" name="stag_heure" value="<?php echo $nb_heure; ?>" /></td>
    <td>
    <button type="button" class="valid-st-stagiaire" <?php if ($stagiaire_id > 0) : ?> style="display: none;"<?php endif; ?>><?php _e("Enregistrer"); ?></button>
    </td>
    <?php return ob_get_clean();
}

add_action( 'wp_ajax_get_st_stagiaires_list', 'get_st_stagiaires_list' );
function get_st_stagiaires_list()
{
    ?>
    <table id="liste-stagiaires">
    <tr>
    <th><?php _e("Nom (facultatif)"); ?></th>
    <th><?php _e("Nombre d'heures"); ?></th>
    </tr>
    <?php
    
    $session_formation = new SessionFormation($_POST['session_id']);
    $inscrits = $session_formation->inscrits;
    if (count($inscrits) > $_POST['nb_stagiaires'])
    {
        $inscrits = array_slice($inscrits, 0, $_POST['nb_stagiaires']);
        $session_formation->update_meta("inscrits", $inscrits);
    }
    else
        $inscrits = array_pad($inscrits, $_POST['nb_stagiaires'], -1);
        
    $session_formation->update_meta("st_nb_stagiaire", count($inscrits));
    
    foreach ($inscrits as $i)
    {
        echo get_st_stagiaire($_POST['session_id'], $_POST['nb_heures'], $i);
    }
    ?>
    </table>
    
    <?php
    die();
}

add_action( 'wp_ajax_update_st_stagiaire', 'update_st_stagiaire' );
function update_st_stagiaire()
{
    $session_id = $_POST['session_id'];
    $user_id = $_POST['user_id'];
    
    $reponse['message'] = "";
    
    if ($user_id < 0)
    {
        $user_id = wp_insert_user
        (
            array('user_nicename' => $_POST['nom'],
                'user_pass' => NULL,
                'user_login' => sanitize_user(strtolower(str_replace(" ", ".", $_POST['nom']))),
                'user_email' => "")
        );
        if (is_wp_error($user_id))
        {
            $reponse['message'] .= "<span class='erreur'>".$user_id->get_error_message()."</span>";
        }
        else
        {
            $user_obj = new WP_User($user_id);
            $user_obj->set_role("um_stagiaire");
            
            if ($session_id > 0)
            {
                $session_formation = new SessionFormation($session_id);
                    
                $session_formation->inscrits[] = $user_id;
                update_post_meta($session_id, 'inscrits', $session_formation->inscrits);
                
                // Ajout de l'ID de la session de formation dans la meta inscription du stagiaire
                // Pas sûr que ce soit toujours utile
                /*
                $inscription = get_user_meta($user_id, "inscription", true);
                if ($inscription == "")
                    $inscription = array();
                
                $inscription[] = $session_id;
                update_user_meta($user_id, "inscription", array_unique($inscription));
                */
            }
        }
            
    }
    else
    {
        $user = new WP_User($user_id);
        $user->user_nicename = $_POST['nom'];
        wp_update_user($user);
    }
    
    $session_stagiaire = new SessionStagiaire($session_id, $user_id);
    $session_stagiaire->update_meta("nb_heure_decimal", $_POST['heures']);
    
    $reponse['html'] = get_st_stagiaire($session_id, $_POST['heures'], $user_id);
    
    echo json_encode($reponse);
    
    die();
}


add_filter('um_profile_tabs', 'add_of_profile_tab', 1000 );
function add_of_profile_tab( $tabs )
{
    $tabs['oftab'] = array
    (
        'name' => 'Formation',
        'icon' => 'um-faicon-graduation-cap',
    );
    return $tabs;
}

/* Then we just have to add content to that tab using this action */

add_action('um_profile_content_oftab_default', 'um_profile_content_oftab_default');
function um_profile_content_oftab_default( $args )
{
    board_profil(um_profile_id());
}


//if (get_current_user_id() > 0)
add_filter('the_content', 'show_profile_box');

function show_profile_box($content)
{
    $user_id = get_current_user_id();
    $role = wpof_get_role($user_id);
    global $role_nom;
    global $wpof;
    
    $gestion_link = "";
    switch ($role)
    {
        case "um_stagiaire":
            $gestion_link = __("Gérer mon dossier");
            break;
        case "um_formateur-trice":
            $gestion_link = __("Gérer mes sessions");
            break;
        default:
            $gestion_link = __("Gérer l'OF");
            break;
    }
    
    ob_start();
    
    ?>
    <div id="profile-box">
    <?php if ($user_id > 0): ?>
    
    <ul id="profile-box-menu">
    <li class="nom"><?php echo get_displayname($user_id, true); ?>
    <span class="role"><?php echo $role_nom[$role]; ?></span></li>
    <li class="gestion-link"><a href="<?php echo home_url(); ?>/user/"><?php echo $gestion_link; ?></a></li>
    <li class="page"><a href="<?php echo home_url(); ?>/formations/"><?php _e("Catalogue de formations"); ?></a></li>
    <li class="page"><a href="<?php echo home_url(); ?>/calendrier/"><?php _e("Calendrier"); ?></a></li>
    <li class="page"><a href="<?php echo home_url(); ?>/equipe-pedagogique/"><?php _e("Équipe pédagogique"); ?></a></li>
    <?php if (in_array($role, array("um_responsable", "admin"))) : ?>
        <li class="bpf-link"><a href="<?php echo home_url().'/'.$wpof->url_bpf; ?>"><?php echo $wpof->title_bpf; ?></a></li>
        <li class="pilote-link"><a href="<?php echo home_url().'/'.$wpof->url_pilote; ?>"><?php echo $wpof->title_pilote; ?></a></li>
    <?php endif; ?>
    <li class="export-link"><a href="<?php echo home_url().'/'.$wpof->url_export; ?>"><?php echo $wpof->title_export; ?></a></li>
    <?php if ($role != "um_stagiaire") : ?>
        <li class="dynamic-dialog" data-function="new_session"><?php _e("Nouvelle session"); ?></li>
    <?php endif; ?>
    <li class="logout"><a href="<?php echo home_url(); ?>/logout/"><?php _e("Déconnexion"); ?></a></li>
    <?php if ($role != 'um_stagiaire') : ?>
    <li class="backend"><a href="<?php echo site_url(); ?>/wp-admin/"><?php _e("Admin du site"); ?></a></li>
    <?php if (debug && $role == "admin") : ?>
        <li class="dynamic-dialog" data-function="debug_SESSION">$_SESSION</li>
        <?php if (get_post_type() == "session") : ?>
                <li class="dynamic-dialog" data-function="sql_session_formation"><?php echo "SQL Session ".get_the_ID(); ?></li>
        <?php endif; ?>
        
    <?php endif; ?>
    </ul>
    <?php endif; ?>
    
    <?php else: ?>
    
    <p class="login"><a href="<?php echo home_url(); ?>/login/"><?php _e("Connexion"); ?></a></p>
    
    <?php endif; ?>
    </div>
    
    <?php
    return $content . ob_get_clean();
}

add_action( 'wp_ajax_switchuser', 'switchuser' );
function switchuser()
{
    $user_id = $_POST['user_id'];

    UM()->user()->auto_login($user_id);
    
    die();
}

add_action( 'wp_ajax_enregistrer_user_input', 'enregistrer_user_input' );
function enregistrer_user_input()
{
    $user_id = $_POST['user_id'];
    $data = $_POST['fields'];
    
    foreach($data as $key => $value)
    {
        $result = update_user_meta($user_id, $key, $value);
        if ($result)
            echo "<span class='succes'>[ ".$key." ] ".__("mise à jour")."</span><br />";
        else
            echo "<span class='alerte'>[ ".$key." ] ".__("inchangée")."</span><br />";
    }
    
    die();
}


?>
