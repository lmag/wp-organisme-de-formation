<?php
/*
 * class-wpof.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */


class WPOF
{
    // Valeurs par défaut
    
    // Aide en ligne
    public $aide_file = "aide";
    public $aide_url = "https://opaga.fr/ressources/";
    
    // Traitement des pages spéciales
    public $special_page = "single";
    public $wpof_of_adresse = "";
    public $wpof_of_code_postal = "";
    public $wpof_of_datadock = "";
    public $wpof_of_description = "";
    public $wpof_of_exotva = "";
    public $wpof_of_hastva = "";
    public $wpof_of_logo = "";
    public $wpof_of_nom = "";
    public $wpof_of_noof = "";
    public $wpof_of_siret = "";
    public $wpof_of_tauxtva = "";
    public $wpof_of_telephone = "";
    public $wpof_of_ville = "";

    public function __construct()
    {
        $all_options = wp_load_alloptions();
        
        $this->aide = $this->init_aide();
        
        foreach ($all_options as $key => $value)
        {
            if (substr($key, 0, 10) == "wpof_aide_")
            {
                $prop = str_replace("-", "_", substr($key, 10));
                $this->aide->$prop = json_decode($value);
            }
            elseif (substr($key, 0, 5) == "wpof_")
            {
                $value = apply_filters( "option_{$key}", maybe_unserialize($value), $key);
                $prop = str_replace("-", "_", substr($key, 5));
                $this->$prop = $value;
            }
        }
    }
    
    public function init_aide()
    {
        $aide_data = file_get_contents(wpof_path . $this->aide_file . ".json");
            
        return json_decode($aide_data);
    }
    
    public function export_aide($format = "json", $display = false)
    {
        $data = "";
        switch ($format)
        {
            case "json":
                $data = json_encode($this->aide, JSON_UNESCAPED_UNICODE);
                break;
            case "html":
                foreach($this->aide as $a)
                {
                    $data .= "<h2>{$a->titre}</h2>";
                    $data .= $a->texte;
                }
                break;
            default:
                break;
        }
        
        if ($display)
            echo $data;
        else
        {
            $file_name = wpof_path . $this->aide_file . "." . $format;
            $file_handle = fopen($file_name, "w");
            fwrite($file_handle, $data);
            fclose($file_handle);
            return $file_name;
        }
    }
}
