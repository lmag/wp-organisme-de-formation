<?php
/*
 * class-session-stagiaire.php
 * 
 * Copyright 2018 Dimitri Robert <dimitri@formation-logiciel-libre.com>
 * 
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * 
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 * 
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
 * MA 02110-1301, USA.
 * 
 * 
 */

require_once(wpof_path . "/class/class-document.php");
require_once(wpof_path . "/class/class-creneau.php");

class SessionStagiaire
{
    public $statut_stagiaire = "";
    public $statut_complement = "";
    public $nature_formation = "";
    public $nature_formation_complement = "";
    public $financement = "";
    public $financement_complement = "";
    public $attentes = "";
    public $etat_session = "initial";
    public $confirme = 0;
    public $temps; // Objet DateTime
    public $nb_heure; // au format horaire : 3:30
    public $nb_heure_decimal = 0; // nombre d'heures au format décimal : 3,5 pour 3:30
    public $tarif_heure = 0;
    public $tarif_total_chiffre = "";
    public $tarif_total_lettre = "";
    public $tarif_base_total = null;  // détermine si le tarif de base défini pour le total (true) ou à l'heure (false)
    public $exe_comptable = array();
    
    // Créneaux de session suivis par le stagiaire
    public $creneaux = array();
    public $dates_array = array();
    public $dates_texte = "";
    
    public $eval_prerequis = array();
    public $eval_preform = array();
    public $eval_postform = array();
    
    public $doc_necessaire = array();
    public $doc_uid = array();
    public $user;
    public $user_id = -1;
    public $session_formation_id;
    public $prenom;
    public $nom;
    public $email;
    public $username;
    public $client_id = -1;
    public $id = -1;
    
    // table suffix
    private $table_suffix;
    


    public function __construct($session_formation_id, $user_id = -1)
    {
        global $SessionFormation;
        global $wpof;
        global $suffix_session_stagiaire;
        
        $this->table_suffix = $suffix_session_stagiaire;
        
        $this->session_formation_id = $session_formation_id;
        
        $session_formation = get_session_by_id($session_formation_id);
        
        if ($user_id > 0)
        {
            $this->id = $session_formation_id."-$user_id";
            $this->user = get_user_by("id", $user_id); // TODO à modifier après avoir créé un vrai objet user
            $this->user_id = $user_id;
            if ($this->user)
            {
                $this->prenom = $this->user->first_name;
                $this->nom = $this->user->last_name;
                $this->email = $this->user->user_email;
                $this->username = $this->user->user_login;
            }
            
            $meta = $this->get_meta();
            
            foreach($meta as $m)
            {
                $this->{$m['meta_key']} = stripslashes($m['meta_value']);
            }
            
            // recherche du numéro de client si absent
            if ($this->client_id == -1)
            {
                $session = get_session_by_id($this->session_formation_id);
                $client_id = reset($session->clients);
                while ($client_id !== false && $this->client_id == -1)
                {
                    $client = get_client_by_id($this->session_formation_id, $client_id);
                    if (in_array($this->user_id, $client->stagiaires))
                        $this->client_id = $client_id;
                    else
                        $client_id = next($session->clients);
                }
            }
            
            if ($this->tarif_base_total == null)
            {
                $this->tarif_base_total = $session_formation->tarif_base_total;
                if ($this->tarif_base_total)
                    $this->tarif_total_chiffre = $session_formation->tarif_total_chiffre;
                else
                {
                    if (!empty($session_formation->tarif_heure))
                        $this->tarif_heure = $session_formation->tarif_heure;
                    else
                        $this->tarif_heure = $wpof->tarif_inter;
                }
            }
/*            
            if (!$this->tarif_base_total && empty($this->tarif_heure))
            {
                if (!empty($session_formation->tarif_heure))
                    $this->tarif_heure = $session_formation->tarif_heure;
                else
                    $this->tarif_heure = $wpof->tarif_inter;
            }
*/                
            
            // documents nécessaires
            foreach($wpof->documents->term as $doc_index => $doc)
            {
                if ($doc->contexte & $wpof->doc_context->stagiaire)
                    $this->doc_necessaire[] = $doc_index;
            }
            $this->doc_suffix = "s".$this->user_id;
            
            if ($session_formation->type_index != "sous_traitance")
            {
                $this->creneaux = array();
                
                // les créneaux actifs sont stockés dans un tableau plat : l'id du créneau en clé et la valeur 0 (inactif) ou 1 (actif)
                $creneaux_from_db = json_decode($this->get_meta("creneaux"), true);
                
                // on définit tous les créneaux comme actifs par défaut ou si $creneaux_from_db[id] n'est pas définit
                if (is_array($session_formation->creneaux))
                    foreach($session_formation->creneaux as $d)
                        foreach($d as $c)
                            $this->creneaux[$c->id] = (isset($creneaux_from_db[$c->id])) ? $creneaux_from_db[$c->id] : 1;

                $this->calcule_temps_session();
                
    /*            {
                    $this->tarif_total_chiffre = $meta['tarif_total_chiffre'];
                    $this->tarif_base_total = true;
                    echo "true";
                }
                else
                {
                    //$this->tarif_total_chiffre = 0;
                    $this->tarif_base_total = false;
    //                echo "false";
    //            }
    */
                /*
                if ($this->tarif_total_chiffre > 0 && $this->nb_heure_decimal > 0 && $this->tarif_heure == 0)
                {
                    $this->tarif_heure = sprintf("%.3f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 3));
                }
                    
                if ($this->tarif_heure == "")
                    $this->tarif_heure = $wpof->tarif_inter;
                */
                $this->calcule_tarif();
                $this->set_exercice_comptable();
            }
        }
    }
    
    /*
     * Création des documents
     * En effet, on n'a pas toujours besoin des documents lorsqu'on instancie une session stagiaire
     */
    public function init_docs()
    {
        global $Documents;
        global $wpof;

        foreach ($this->doc_necessaire as $d)
        {
            $doc = new Document($d, $this->session_formation_id, $wpof->doc_context->stagiaire, $this->user_id);
            $Documents[$doc->id] = $doc;
            $this->doc_uid[] = $doc->id;
        }
    }
    
    /*
     * Retourne une métadonnée (ou toutes) de session stagiaire
     * $meta_key : clé de la valeur recherchée
     *
     * Retourne la valeur recherchée ou un tableau de toutes les valeurs
     */
    public function get_meta($meta_key = null)
    {
        return get_stagiaire_meta($this->session_formation_id, $this->user_id, $meta_key);
    }

    public function update_meta($meta_key, $meta_value = null)
    {
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        if ($meta_value == null)
            $meta_value = $this->$meta_key;
        else
            $this->$meta_key = $meta_value;
            
        switch ($meta_key)
        {
            case "prenom":
                $res = update_user_meta($this->user_id, "first_name", $meta_value);
                break;
            case "nom":
                $res = update_user_meta($this->user_id, "last_name", $meta_value);
                break;
            case "email":
                $res = wp_update_user(array('ID' => $this->user_id, 'user_email' => $meta_value));
                break;
            default:
                if (is_array($meta_value))
                    $meta_value = serialize($meta_value);
                
                $query = $wpdb->prepare
                ("INSERT INTO $table (session_id, user_id, meta_key, meta_value)
                    VALUES ('%d', '%d', '%s', '%s')
                    ON DUPLICATE KEY UPDATE meta_value = '%s';",
                    $this->session_formation_id, $this->user_id, $meta_key, $meta_value, $meta_value);
                
                $res = $wpdb->query($query);
                break;
        }
        
        return $res;
    }
    
    // Renseigne le tableau des exercices comptables
    // En général ce tableau n'aura qu'une valeur (si toute la session se déroule dans la même année civile)
    public function set_exercice_comptable()
    {
        global $SessionFormation;
        if ($SessionFormation[$this->session_formation_id]->type_index == 'inter')
        {
            $this->exe_comptable = unserialize($this->get_meta("exe_comptable"));
            
            if (!is_array($this->exe_comptable) || count($this->exe_comptable) == 0)
            {
                $this->exe_comptable = array();
                foreach($this->dates_array as $d)
                {
                    $date = explode('/', $d);
                    $this->exe_comptable[$date[2]] = 0;
                }
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] = $this->tarif_total_chiffre;
                
                $this->update_meta("exe_comptable");
            }
            $diff = $this->tarif_total_chiffre - array_sum($this->exe_comptable);
            if ($diff != 0)
            {
                $last_year = array_keys($this->exe_comptable);
                $last_year = $last_year[count($last_year) - 1];
                $this->exe_comptable[$last_year] += $diff;
                
                $this->update_meta("exe_comptable");
            }
        }
    }
    
    
    // Calcule le temps (en heures) de suis de la session par ce stagiaire (somme de tous les créneaux actifs)
    public function calcule_temps_session()
    {
        $session_formation = get_session_by_id($this->session_formation_id);
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        $this->temps = DateTime::createFromFormat("U", "0");
        $this->dates_array = array();
        
        foreach($session_formation->creneaux as $date => $tab_date)
            foreach($tab_date as $creno)
            {
                if (!isset($this->creneaux[$creno->id]) || !isset($client->creneaux[$creno->id]) || $this->creneaux[$creno->id] * $client->creneaux[$creno->id] != 0)
                {
                    if ($creno->type != "technique")
                        $this->temps->add($creno->duree);
                    $this->dates_array[] = $date;
                }
            }
        
        $this->dates_array = array_unique($this->dates_array);
        $this->dates_texte = pretty_print_dates($this->dates_array);
        
        $h = $this->temps->format("U") / 3600;
        $m = ($this->temps->format("U") % 3600) / 60;
        $this->nb_heure_decimal = $this->temps->format("U") / 3600;
        $this->nb_heure = sprintf("%02d:%02d", $h, $m);
        
        if (!isset($this->nb_heure_estime_decimal))
        {
            $this->nb_heure_estime_decimal = $this->nb_heure_decimal;
            $this->nb_heure_estime = $this->nb_heure;
        }
        else
        {
            $h = (integer) $this->nb_heure_estime_decimal;
            $m = ($this->nb_heure_estime_decimal - $h) * 60;
            $this->nb_heure_estime = sprintf("%02d:%02d", $h, $m);
        }
    }
    
    public function calcule_tarif()
    {
        if ($this->tarif_base_total == 1)
        {
            if ($this->nb_heure_decimal > 0)
                $this->tarif_heure = sprintf("%.2f", round($this->tarif_total_chiffre / $this->nb_heure_decimal, 2));
        }
        else
            $this->tarif_total_chiffre = sprintf("%.2f", round($this->tarif_heure * $this->nb_heure_decimal, 1));
        $this->tarif_total_lettre = num_to_letter($this->tarif_total_chiffre);
    }
    
    /*
     * Suppression du stagiaire pour cette session
     */
    public function delete()
    {
        // supression du client du tableau inscrits de la session TODO : supprimer ce tableau définitivement
        $session = get_session_by_id($this->session_formation_id);
        $session->supprime_sous_entite("inscrits", $this->user_id);
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        $client->supprime_sous_entite("stagiaires", $this->user_id);
        
        // suppression du stagiaire dans la base de données
        global $wpdb;
        
        $table = $wpdb->prefix . $this->table_suffix;
        
        $query = $wpdb->prepare
        ("DELETE FROM $table
            WHERE user_id = '%d' AND session_id = '%d';",
            $this->user_id, $this->session_formation_id
        );
        
        return $wpdb->query($query);
    }
    
    /*
     * Interface avec le stagiaire
     */
    public function the_interface()
    {
        echo $this->get_the_interface();
    }
    
    public function get_the_interface()
    {
        global $wpof;
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        
        echo hidden_input("default_main_tab", (isset($_SESSION['main-tabs'])) ? $_SESSION['main-tabs'] : 0);
        ?>
        <div id="main-tabs">
            <ul>
                <li><a href="#tab-initial"><?php _e("Avant la formation"); ?></a></li>
                <li><a href="#tab-documents"><?php _e("Documents"); ?></a></li>
                <li><a href="#tab-apres"><?php _e("Après la formation"); ?></a></li>
            </ul>
            
            <div id="tab-initial" class="stagiaire" data-userid="<?php echo $this->user_id; ?>">
                <form class="notif-modif">
                <h2><?php _e("Vos besoins"); ?></h2>
                <p><?php _e("Quelles sont vos attentes par rapport à cette formation ? Comment allez-vous mettre en pratique ce que vous allez apprendre ?"); ?></p>
                <textarea cols='60' rows='15' name='attentes'><?php echo $this->attentes; ?></textarea>
                
                <?php
                    echo "<h2>{$session_formation->quizpr->sujet_titre}</h2>";
                    echo $session_formation->quizpr->get_html($this->user_id, $session_formation->quizpr_id, 0);
                    echo "<h2>{$session_formation->quizobj->sujet_titre} avant la formation</h2>";
                    echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 0);
                ?>
                <div class='icone-bouton submit stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
                </form>
                <p class="message"></p>
            </div> <!-- initial -->
            
            <div id="tab-documents">
                <?php
                global $Documents;
                $this->init_docs();
                $liste_doc = "";
                foreach ($this->doc_uid as $docuid)
                {
                    $doc = $Documents[$docuid];
                    if ($doc->visible_stagiaire)
                        $liste_doc .= "<li class='doc {$doc->type}'>{$doc->link_name}</li>";
                }
                if ($liste_doc != "") : ?>
                <ul class="liste-doc-stagiaire">
                <?php echo $liste_doc; ?>
                </ul>
                <?php else : ?>
                <p><?php echo $wpof->doc_pas_de_docs; ?></p>
                <?php endif; ?>
            </div>
        
            <div id="tab-apres">
            <form>
            <?php
                echo "<h2>{$session_formation->quizobj->sujet_titre} après la formation</h2>";
                echo $session_formation->quizobj->get_html($this->user_id, $session_formation->quizobj_id, 1);
            ?>
            <div class='icone-bouton stagiaire-submit'><span class='dashicons dashicons-plus-alt'></span><?php _e("Enregistrez vos modifications"); ?></div>
            </form>
            <p class="message"></p>
            </div>
        
        <?php
        return ob_get_clean();
    }
    
    /*
     * Tableau de bord stagiaire, pour le formateur
     */
    public function the_board()
    {
        echo $this->get_the_board();
    }
    public function get_the_board()
    {
        $current_user_id = get_current_user_id();
        $role = wpof_get_role($current_user_id);
        
        if ($role == "um_stagiaire")
            return $this->get_the_interface();
        
        $client = get_client_by_id($this->session_formation_id, $this->client_id);
        
        global $doc_nom;
        global $wpof;
        global $Documents;
        $this->init_docs();
        
        $session_formation = get_session_by_id($this->session_formation_id);
        
        ob_start();
        ?>
        <div class='board board-stagiaire edit-data client-<?php echo $this->client_id; ?> stagiaire-<?php echo $this->user_id; ?>' id='tab-s<?php echo $this->user_id; ?>'>
            <div class="infos-stagiaire flexrow">
            <?php
            ?>
                <div>
                <?php
                    echo get_input_jpost($this, "prenom", array('input' => 'text', 'label' => __("Prénom")));
                    echo get_input_jpost($this, "nom", array('input' => 'text', 'label' => __("Nom")));
                    echo get_input_jpost($this, "email", array('input' => 'text', 'label' => __("Adresse email")));
                    echo get_input_jpost($this, "statut_stagiaire", array('select' => '', 'label' => __("Statut du stagiaire")));
                    echo get_input_jpost($this, "nb_heure_estime_decimal", array('input' => 'number', 'label' => __("Durée estimée en heures (en décimal)"))); 
                    echo get_input_jpost($this, "confirme", array('input' => 'checkbox', 'label' => __("Inscription confirmée")));
                ?>
                </div>
                <div class="icones">
                <p class="delete-entity icone-bouton" data-objectclass="<?php echo get_class($this); ?>" data-id="<?php echo $this->user_id; ?>" data-sessionid="<?php echo $this->session_formation_id; ?>" data-parent=".stagiaire-<?php echo $this->user_id; ?>">
                <span class="dashicons dashicons-dismiss" > </span>
                <?php _e("Supprimer ce stagiaire"); ?>
                </p>
                
                <?php if ($role == "admin"): ?>
                <p>ID : <?php echo $this->id; ?></p>
                <p>Client ID : <?php echo $this->client_id; ?></p>
                <p><span data-userid='<?php echo $this->user_id ?>' data-url='<?php echo $session_formation->permalien; ?>' class='bouton switch-user dashicons dashicons-controls-repeat'></span></p>
                <?php endif; ?>
                
                <p><span class='last-modif'><?php echo __("Dernière connexion")." ".get_last_login($this->user_id); ?></span></p>
                </div>
            </div> <!-- infos-stagiaire -->
            
            <fieldset>
                <legend><?php _e("Documents administratifs pour la session"); ?></legend>
                <?php echo get_gestion_docs($this); ?>
            </fieldset>
                
            <?php if (0) : ?>
            <fieldset><legend><?php _e("Créneaux de présence"); ?></legend>
            <?php
                if (count($session_formation->creneaux) == 0)
                    echo "<p class='alerte'>".__("Définissez d'abord des créneaux")."</p>";
                else
                    echo $session_formation->get_html_creneaux(false, $this);
            ?>
            </fieldset>
            <?php endif; ?>
            <fieldset>
                <legend><?php _e("Évaluations et besoins"); ?></legend>
                <?php echo $this->get_bilan_eval(); ?>
            </fieldset>
                
        </div> <!-- board-stagiaire -->
        <?php
        return ob_get_clean();
    }
    
    public function get_bilan_eval()
    {
        $session = get_session_by_id($this->session_formation_id);
        $html = "";
        
        $html .= get_input_jpost($this, "attentes", array('textarea' => 1, 'rows' => '10', 'cols' => '60', 'label' => __("Attentes, besoins, motivations")));
        
        $html .= "<div class='radio-disable'>";
        if (!empty($session->quizpr->sujet))
        {
            $html .= "<h2>{$session->quizpr->sujet_titre}</h2>";
            $html .= $session->quizpr->get_html($this->user_id, $session->quizpr_id, 0);
        }
        if (!empty($session->quizobj->sujet))
        {
            $html .= "<h2>{$session->quizobj->sujet_titre} avant la formation</h2>";
            $html .= $session->quizobj->get_html($this->user_id, $session->quizobj_id, 0);
            $html .= "<h2>{$session->quizobj->sujet_titre} après la formation</h2>";
            $html .= $session->quizobj->get_html($this->user_id, $session->quizobj_id, 1);
        }
        $html .= "</div>";
        
        return $html;
    }
}

function get_stagiaire_meta($session_id, $stagiaire_id, $meta_key)
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    if ($meta_key)
    {
        $query = $wpdb->prepare
        ("SELECT meta_value
            from $table_session_stagiaire
            WHERE session_id = '%d'
            AND user_id = '%d'
            AND meta_key = '%s';",
            $session_id, $stagiaire_id, $meta_key);
        return $wpdb->get_var($query);
    }
    else
    {
        $query = $wpdb->prepare("SELECT meta_key, meta_value from $table_session_stagiaire WHERE session_id = '%d' AND user_id = '%d';", $session_id, $stagiaire_id);
        return $wpdb->get_results($query, ARRAY_A);
    }
}

function get_stagiaire_by_id($session_id, $user_id)
{
    global $SessionStagiaire;
    
    if (!isset($SessionStagiaire[$user_id]))
        $SessionStagiaire[$user_id] = new SessionStagiaire($session_id, $user_id);
        
    return $SessionStagiaire[$user_id];
}

function get_all_stagiaires()
{
    global $wpdb;
    global $suffix_session_stagiaire;
    $table_session_stagiaire = $wpdb->prefix.$suffix_session_stagiaire;
    
    $stagiaires = array();
    $result = $wpdb->get_results("SELECT DISTINCT session_id, user_id FROM $table_session_stagiaire;");
    if (is_array($result))
        foreach($result as $row)
        {
            $s = new SessionStagiaire($row->session_id, $row->user_id);
            if (!empty($s->prenom))
                $stagiaires[] = $s;
        }
    
    return $stagiaires;
}
